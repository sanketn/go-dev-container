## go-dev-container ##

This container allows the user to use it as a development environment for golang projects.


#### How to use ? ####

```
docker container create --name godev -v "$(PWD)/project:/home/project" sanketnaik/go-dev-container
docker container start godev
```
The above command should create your containers and using the below command you can start the same. 

```
PLEASE NOTE: Prior to starting the container, please create the respective folder: $(PWD)/project
```

#### How to use  it with compose ? ####

```
version: '3'
volumes:
  govol:
    driver: local
    driver_opts:
      o: bind
      type: none
      device: $HOME/project

services:
  godev:
    image: sanketnaik/go-dev-container
    volumes:
      - govol:/home/project
```
The above is a sample compose file. 

```
PLEASE NOTE: Prior to starting the containers, please create the respective folder: $HOME/project
```

#### Using for Developoment with vscode ####

Once the stack/container is up, then you can follow the instructions [here](https://code.visualstudio.com/docs/remote/containers#_attaching-to-running-containers) to attach Visual Studio Code and use this container as a development environment.
